import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import Toaster from '@meforma/vue-toaster';

axios.defaults.baseURL = 'https://coffee-trent.herokuapp.com'
createApp(App).use(store).use(router).use(Toaster).mount('#app')