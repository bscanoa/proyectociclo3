import { createRouter, createWebHistory } from 'vue-router'
import App from './App.vue'

import store from './store'

import SignUp from './views/SignUp.vue'
import Checkout  from './views/Checkout.vue'
import LogIn from './views/LogIn.vue'
import Home from './views/Home.vue'
import Producto from './views/Producto.vue'
import AllProducts from './views/AllProducts.vue'
import Category from './views/Category.vue'
import Search from './views/Search.vue'
import Cart from './views/Cart.vue'
import Account from './views/Account.vue'
import Success from './views/Success.vue'
import Nosotros from './views/Nosotros.vue'

const routes = [{
  path: '/',
  name: 'home',
  component: Home
},
{
  path: '/user/logIn',
  name: "logIn",
  component: LogIn
},
{ 
  path: '/user/signUp',
  name: "signUp",
  component: SignUp
},
{
  path: '/user/account',
  name: "account",
  component: Account
},
{
  path: '/:category_slug/:product_slug',
  name: 'Product',
  component: Producto
},
{
  path: '/productos',
  name: 'AllProducts',
  component: AllProducts
},
{
  path: '/:category_slug',
  name: 'Category',
  component: Category
},
{
  path: '/search',
  name: 'Search',
  component: Search
},
{
  path: '/carrito',
  name: 'Cart',
  component: Cart
},
{
  path: '/user',
  name: 'Account',
  component: Account,
  meta: {
    requireLogin: true
  }
},
{
  path: '/carrito/pago',
  name: 'Checkout',
  component: Checkout,
  meta: {
      requireLogin: true
  }
},
{
  path: '/carrito/hecho',
  name: 'Success',
  component: Success
},
{
  path: '/nosotros',
  name: 'Nosotros',
  component: Nosotros
},
]
const router = createRouter({
  history: createWebHistory(),
  routes
})
router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requireLogin) && !store.state.isAuthenticated) {
    next({ name: 'LogIn', query: { to: to.path } });
  } else {
    next()
  }
})

export default router

